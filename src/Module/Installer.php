<?php

namespace Spirate\Composer\Module;

use Composer\Package\PackageInterface;
use Composer\Installer\LibraryInstaller;


class Installer extends LibraryInstaller
{
    /**
     * {@inheritDoc}
     */
    public function getInstallPath(PackageInterface $package)
    {
        $extra = $package->getExtra();
        $prettyName = $package->getPrettyName();
        $moduleName = $this->normalizeModuleName($extra['module-name']);

        if (!isset($moduleName)) {
            throw new \InvalidArgumentException(
                'Unable to install module, missing "module-name" option in "extra" object'
            );
        }

        // TODO: check valid module folder name

        return 'modules/' . $moduleName;
    }

    /**
     * {@inheritDoc}
     */
    public function supports($packageType)
    {
        return 'spirate-module' === $packageType;
    }

    /**
     * Get a normalized module name
     *
     * @param $name
     * @return string
     */
    private function normalizeModuleName($name)
    {
        $name = str_replace([
            '-',
            '_'
        ], '', $name);

        $name = ucwords(strtolower($name));
        $name = str_replace(' ', '', $name);

        return $name;
    }
}